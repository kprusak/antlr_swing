grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat) + EOF!;

stat
    : expr NL -> expr

    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | PRINT expr NL -> ^(PRINT expr)
    | ID PODST expr NL -> ^(PODST ID expr)
    | if_statement NL -> if_statement
    | LB
    | RB
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;


if_statement
    : IF^ LP! condition=expr RP! LB! positive=expr RB! (ELSE! LB! negative=expr RB!)?
    ;

multExpr
    : powExpr
      ( MUL^ powExpr
      | DIV^ powExpr
      )*
    ;

powExpr
    : atom 
      ( POW^ atom
      | SQRT^
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;


PRINT : 'print';

VAR :'var';

IF : 'if' ;

ELSE : 'else' ;

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
	:	'('
	;

RP
	:	')'
	;

LB
  : '{'
  ;
  
RB
  : '}'
  ;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
	
POW
  : '^'
  ; 

SQRT
  : '#'
  ;  
  

