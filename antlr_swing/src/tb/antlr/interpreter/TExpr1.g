tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}
prog    : (expr | vars | output)*;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = substract($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = multiply($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out, $e2.out);}
        | ^(SQRT  e1=expr)         {$out = sqrt($e1.out);}
        | ^(PODST i1=ID   e2=expr) {setVar($i1.text, $e2.out);}    
        | ID {$out = getID($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        | LB {enterScope();}
        | RB {leaveScope();}
        ;

vars    : ^(VAR i1=ID)             {newVar($i1.text);}
          ^(PODST i1=ID   e2=expr) {setVar($i1.text, $e2.out);}
        ;     

output  : ^(PRINT e=expr)          {drukuj($e.text + " = " + $e.out.toString());}
        ;
        
 
catch [Exception ex] {
  System.out.println("Error: " + ex.getMessage());
}
 