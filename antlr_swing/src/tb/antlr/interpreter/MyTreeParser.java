package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.*;

public class MyTreeParser extends TreeParser {

	private LocalSymbols localSymbols = new LocalSymbols();
	
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	//funkcja dodawania
	protected Integer add(Integer e1, Integer e2) {
		return e1 + e2;
	}
	
	//funkcja odejmowania
	protected Integer substract(Integer e1, Integer e2) {
		return e1 - e2;
	}

	//funkcja mnożenia
	protected Integer multiply(Integer e1, Integer e2) {
		return e1 * e2;
	}

	//funkcja dzielenia
	protected Integer divide(Integer e1, Integer e2) throws RuntimeException {
		if(e2 == 0)	throw new RuntimeException("Dividing by 0 is not allowed!");
		return e1 / e2;
	}

	//funkcja potegowania
	protected Integer power(Integer e1, Integer e2) {
		return (int) Math.pow(e1, e2);
	}

	//funkcja pierwiastkowania (pierwiastek kwadratowy)
	protected Integer sqrt(Integer e1) {
		return (int) Math.sqrt(e1);
	}
	
	
	//zmienne i zakresy
	 protected void enterScope() {
		 localSymbols.enterScope();
	 }

	 protected void leaveScope() {
		 localSymbols.leaveScope();
	 }	

	 protected void newVar(String varName) {
		localSymbols.newSymbol(varName);
	 }

	 protected void setVar(String varName, Integer value) {
		localSymbols.setSymbol(varName, value);
	 }

	 protected Integer getID(String varName) {
		if(localSymbols.hasSymbol(varName)) {
		return localSymbols.getSymbol(varName);
		}
		System.out.println("This variable doesn't exist. Declare it first with keyword \"var\"! Default value = 0");
		return 0;
	 }
	    
}
